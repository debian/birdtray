<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>WindowsInstaller</name>
    <message>
        <source>__LANGUAGE_NAME__</source>
        <translatorcomment>This must be the name of one of the NSIS translation files at https://sourceforge.net/p/nsis/code/HEAD/tree/NSIS/trunk/Contrib/Language%20files/ without the file extension.</translatorcomment>
        <translation>Polish</translation>
    </message>
    <message>
        <source>BirdtraySectionDescription</source>
        <translation>Darmowe powiadomienia o nowej poczcie dla Thunderbirda.</translation>
    </message>
    <message>
        <source>WinIntegrationSectionName</source>
        <translation>Integracja z Windows</translation>
    </message>
    <message>
        <source>WinIntegrationGroupDescription</source>
        <translation>Wybierz jak chcesz zintegrować program z Windows.</translation>
    </message>
    <message>
        <source>AutoRunSectionName</source>
        <translation>Samoczynne uruchomienie</translation>
    </message>
    <message>
        <source>AutoRunDescription</source>
        <translation>Uruchom automatycznie ${PRODUCT_NAME} po zalogowaniu.</translation>
    </message>
    <message>
        <source>AutoCheckUpdateSectionName</source>
        <translation>Automatyczne sprawdzanie aktualizacji</translation>
    </message>
    <message>
        <source>AutoCheckUpdateDescription</source>
        <translation>Automatycznie sprawdzaj aktualizacje ${PRODUCT_NAME} przy uruchomieniu.</translation>
    </message>
    <message>
        <source>ProgramGroupSectionName</source>
        <translation>Wpis Grupy Programów</translation>
    </message>
    <message>
        <source>ProgramGroupDescription</source>
        <translation>Stwórz ${PRODUCT_NAME} grupę programów w Menu Start > Programy.</translation>
    </message>
    <message>
        <source>DesktopEntrySectionName</source>
        <translation>Skrót na pulpicie</translation>
    </message>
    <message>
        <source>DesktopEntryDescription</source>
        <translation>Stwórz ${PRODUCT_NAME} skrót na pulpicie.</translation>
    </message>
    <message>
        <source>StartMenuSectionName</source>
        <translation>Stwórz skrót w Menu Start</translation>
    </message>
    <message>
        <source>StartMenuDescription</source>
        <translation>Stwórz ${PRODUCT_NAME} skrót w Menu Start.</translation>
    </message>
    <message>
        <source>TranslationsSectionName</source>
        <translation>Tłumaczenia</translation>
    </message>
    <message>
        <source>TranslationsDescription</source>
        <translation>Wybierz tłumaczenie ${PRODUCT_NAME}. ${PRODUCT_NAME} na podstawie języka systemu.</translation>
    </message>
    <message>
        <source>UserSettingsSectionName</source>
        <translation>${PRODUCT_NAME} Ustawienia użytkownika</translation>
    </message>
    <message>
        <source>UserSettingsDescription</source>
        <translation>Twoje ustawienia i konfiguracja z ${PRODUCT_NAME}.</translation>
    </message>
    <message>
        <source>LicenseStartMenuLinkName</source>
        <translation>Postanowienia licencji</translation>
    </message>
    <message>
        <source>FinishPageRunBirdtray</source>
        <translation>Uruchom ${PRODUCT_NAME}</translation>
    </message>
    <message>
        <source>UninstallerConfirmation</source>
        <translation>Naciśnij odinstaluj, aby rozpocząć proces.</translation>
    </message>
    <message>
        <source>UninstallPreviousVersion</source>
        <translation>Odinstalowywanie poprzedniej wersji ${PRODUCT_NAME}</translation>
    </message>
    <message>
        <source>UninstallPreviousVersionError</source>
        <translation>Odinstalowywanie starej wersji ${PRODUCT_NAME} nie powiodło się! Kontynuacja usunie WSZYSTKO w $3.</translation>
    </message>
    <message>
        <source>UninstallExecutionError</source>
        <translation>Błąd przy uruchamianiu deinstalatora.</translation>
    </message>
    <message>
        <source>StopBirdtray</source>
        <translation>${PRODUCT_NAME} aktualnie działa. Naciśnij OK aby zatrzymać ${PRODUCT_NAME} i kontynuować instalację.</translation>
    </message>
    <message>
        <source>StoppingBirdtray</source>
        <translation>Zamykanie ${EXE_NAME}</translation>
    </message>
    <message>
        <source>StopBirdtrayError</source>
        <translation>Nie udało się zamknąć ${PRODUCT_NAME}. Spróbuj ponownie, zamknij ${PRODUCT_NAME} ręcznie, aby kontynuować instalację.</translation>
    </message>
    <message>
        <source>StopBirdtrayUninstall</source>
        <translation>${PRODUCT_NAME} aktualnie działa. Naciśnij OK aby zatrzymać ${PRODUCT_NAME} i kontynuować deinstalację.</translation>
    </message>
    <message>
        <source>StopBirdtrayUninstallError</source>
        <translation>Nie udało się zamknąć ${PRODUCT_NAME}. Spróbuj ponownie, zamknij ${PRODUCT_NAME} ręcznie, aby kontynuować deinstalację.</translation>
    </message>
    <message>
        <source>SetupAlreadyRunning</source>
        <translation>Instalacja ${PRODUCT_NAME} aktualnie działa.$\r$\nZamknij wszystkie instancje programu, aby kontynuować, lub Anuluj, aby wyjść.</translation>
    </message>
    <message>
        <source>UninstallAlreadyRunning</source>
        <translation>Deinstalacja ${PRODUCT_NAME} aktualnie działa.$\r$\nZamknij wszystkie instancje programu, aby kontynuować, lub Anuluj, aby wyjść.</translation>
    </message>
    <message>
        <source>BadInstallPath</source>
        <translation>Ścieżka docelowa nie może zawierać:$\r$\n${BAD_PATH_CHARS}.</translation>
    </message>
    <message>
        <source>InstallPathContainsPreviousVersion</source>
        <translation>Folder docelowy zawiera poprzednią wersję ${PRODUCT_NAME}, która zostanie zastąpiona.</translation>
    </message>
    <message>
        <source>InstallPathNotEmpty</source>
        <translation>Folder docelowy nie jest pusty, ale nie zawiera poprzedniej wersji ${PRODUCT_NAME}. Zawartość folderu docelowego zostanie nadpisana.</translation>
    </message>
    <message>
        <source>CurrentUserOnly</source>
        <translation>$0 (Tylko aktualny użytkownik)</translation>
    </message>
    <message>
        <source>InstallFailed</source>
        <translation>${PRODUCT_NAME} ${VERSION} nie może zostać zainstalowana.$\r$\n\Uruchom ponownie Windows i ponów instalację.</translation>
    </message>
    <message>
        <source>InstallFailedCauseUninstallerFailed</source>
        <translation>${PRODUCT_NAME} nie może zostać zainstalowana.$\r$\n\Uruchom ponownie Windows i ponów instalację.</translation>
    </message>
    <message>
        <source>UninstallFailed</source>
        <translation>${PRODUCT_NAME} ${VERSION} nie może zostać odinstalowana.$\r$\nPUruchom ponownie Windows i ponów deinstalację.</translation>
    </message>
    <message>
        <source>FileDeleteErrorDialog</source>
        <translation>Błąd podczas usuwania pliku:$\r$\n$\r$\n$0$\r$\n$\r$\nNaciśnij ponów, aby spróbować ponownie, lub$\r$\nAnuluj, aby zatrzymać proces.</translation>
    </message>
    <message>
        <source>FileDeleteError</source>
        <translation>Błąd podczas usuwania pliku $0</translation>
    </message>
    <message>
        <source>Install64On32BitWarning</source>
        <translation>Próbujesz zainstalować 32-bitową wersję ${PRODUCT_NAME} na 64-bitowym systemie. Jest 64-bitowa wersja ${PRODUCT_NAME} dostępna do pobrania.</translation>
    </message>
    <message>
        <source>Install32On64BitError</source>
        <translation>Instalator zawiera 64-bitową wersję ${PRODUCT_NAME}. Twój komputer działa pod kontrolą 32-bitowego systemu operacyjnego, i nie może wykonywać 64-bitowych programów. Pobierz 32-bitowy instalator ${PRODUCT_NAME}.</translation>
    </message>
    <message>
        <source>UnsupportedWindowsVersionError</source>
        <translation>Program wymaga co najmniej Windowsa ${MIN_WINDOWS_VER}.</translation>
    </message>
    <message>
        <source>UninstallerAborted</source>
        <translation>Deinstalacja ${PRODUCT_NAME} została przerwana. Spróbuj ponownie później.</translation>
    </message>
    <message>
        <source>SettingsLink</source>
        <translation>ustawienia ${PRODUCT_NAME}</translation>
    </message>
    <message>
        <source>UninstallRestartAsUserQuestion</source>
        <translation>Deinstalator działa z uprawnieniami administratora, ale nie znaleziono ${PRODUCT_NAME} dla administratora. Czy chcesz uruchomić ponownie deinstalator bez uprawnień administratora?</translation>
    </message>
    <message>
        <source>OpenUrl</source>
        <translation>Otwieranie $0 w domyślnej przeglądarce...</translation>
    </message>
    <message>
        <source>MissingVcRuntime</source>
        <translation>Nie można znaleźć Visual C++ Runtime ${ARCH}.</translation>
    </message>
    <message>
        <source>MissingVcRuntime_Dialog</source>
        <translation>Nie znaleziono wymaganego komponentu:$\r$\nVisual C++ Runtime ${ARCH}$\r$\nCzy chcesz pobrać jego instalator?</translation>
    </message>
    <message>
        <source>MissingVcRuntime_Retry</source>
        <translation>Kiedy pobieranie się zakończy uruchom pobrany instalator. Kiedy instalacja się zakończy naciśnij OK, aby sprawdzić ponownie status instalacji.</translation>
    </message>
    <message>
        <source>MissingVcRuntime_Found</source>
        <translation>Wymagany komponent został znaleziony na komputerze.</translation>
    </message>
    <message>
        <source>MissingVcRuntime_StillNotFound</source>
        <translation>Nie znaleziono wymaganego komponentu. Poszukaj w sieci Visual C++ ${ARCH} dla Visual Studio 2015 lub wyższej.</translation>
    </message>
    <message>
        <source>MissingVcRuntime_UnableToRun</source>
        <translation>${PRODUCT_NAME} nie będzie możliwy do uruchomienia.</translation>
    </message>
    <message>
        <source>MissingVcRuntime_UnableToRunDialog</source>
        <translation>${PRODUCT_NAME} nie może zostać uruchomiony z powodu braku wymaganego komponentu.</translation>
    </message>
    <message>
        <source>Lang_en</source>
        <translation>angielski</translation>
    </message>
    <message>
        <source>Lang_de</source>
        <translation>niemiecki</translation>
    </message>
    <message>
        <source>Lang_nl</source>
        <translation>niderlandzki</translation>
    </message>
    <message>
        <source>Lang_it</source>
        <translation>włoski</translation>
    </message>
    <message>
        <source>Lang_ru</source>
        <translation>rosyjski</translation>
    </message>
    <message>
        <source>Lang_pt</source>
        <translation>portugalski</translation>
    </message>
    <message>
        <source>Lang_es</source>
        <translation>hiszpański</translation>
    </message>
    <message>
        <source>Lang_sv</source>
        <translation>szwedzki</translation>
    </message>
    <message>
        <source>Lang_tr</source>
        <translation>turecki</translation>
    </message>
    <message>
        <source>Lang_pl</source>
        <translation>polski</translation>
    </message>
</context>
</TS>
